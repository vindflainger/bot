package main

import (
	"PIE_BOT/Code/commands/chat"
	mymath "PIE_BOT/Code/commands/math"
	"PIE_BOT/Code/longpoll"
	"database/sql"
	"errors"
	"fmt"
	"github.com/go-vk-api/vk"
	_ "github.com/lib/pq"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"
)

const (
	host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = "gemger2003"
	dbname   = "testdb"
)

type MathG struct {
	Numbers     map[int][][]int
	Active      bool
	Range       int
	Samplecount int
}

func mathrunner(client *chat.VKClient, db *sql.DB, object <-chan *longpoll.VKObject) {
	var mesid int64
	mathg := MathG{Active: false, Numbers: map[int][][]int{}}
	for vkobject := range object {
		text := strings.Split(vkobject.Message.Text, " ")

		switch text[0] {
		case "-старт": // -start range samplecount pickcount
			if vkobject.Message.FromID != 251854093 || mathg.Active {
				client.SendDefaultUsMessage(vkobject.Message.FromID, "", &mesid, []interface{}{"У вас нет доступа или процесс уже запущен"})
				continue
			}

			if len(text) != 3 {
				client.SendDefaultUsMessage(vkobject.Message.FromID, "", &mesid, []interface{}{"Ошибка формата ввода"})
				continue
			}

			rang, err := strconv.Atoi(text[1])
			samplecount, err := strconv.Atoi(text[2])

			if err != nil {
				client.SendDefaultUsMessage(vkobject.Message.FromID, "", &mesid, []interface{}{"Ошибка формата команды"})
				continue
			}

			chances := make(map[int]float64)
			mathg.Active = true
			mathg.Range = rang
			mathg.Samplecount = samplecount

			for i := 1; i < mathg.Samplecount+1; i++ {
				chances[i] = mymath.GuessChanse(mathg.Range, mathg.Samplecount, i)
			}

			pic, _ := client.GetSaveImage("loto.jpg")
			client.SendPhotoUsMessage(vkobject.Message.FromID, "", &mesid, pic,
				[]interface{}{fmt.Sprintf(
					"Лотарея успешно запущена со следующими параметрами\nКол-во чисел: %d\nРазмер выборки: %d\nШансы победы: \n ",
					rang,
					samplecount,
				), chances})

		case "-билетик": // -take 1/1/1/2/4
			if !mathg.Active {
				client.SendDefaultUsMessage(vkobject.Message.FromID, "", &mesid, []interface{}{"Лотарея еще не активна"})
				continue
			}

			var nums []int
			strnums := strings.Split(strings.Trim(text[1], "/"), "/")
			if len(strnums) > mathg.Samplecount {
				client.SendDefaultUsMessage(vkobject.Message.FromID, "", &mesid, []interface{}{fmt.Sprintf("Максимальное кол-во чисел - %d", mathg.Samplecount)})
				continue
			}
			if err := func() error {
				for _, strnum := range strnums {
					parsenum, err := strconv.Atoi(strnum)
					if err != nil {
						return errors.New("Ошибка формата, нужный формат: -take 1/2/3/4/5")

					}
					if parsenum > mathg.Range || parsenum <= 0 {
						return errors.New(fmt.Sprintf("Числа должно быть из интервала [1:%d]", mathg.Range))
					}
					for _, num := range nums {
						if num == parsenum {
							return errors.New("Нельзя брать одно и то же число")
						}
					}
					nums = append(nums, parsenum)
				}
				return nil
			}(); err != nil {
				client.SendDefaultUsMessage(vkobject.Message.FromID, "", &mesid, []interface{}{err.Error()})
				continue
			}

			points := mymath.CheckAvail(vkobject.Message.FromID, db)
			if points <= 0 {
				client.SendDefaultUsMessage(vkobject.Message.FromID, "", &mesid, []interface{}{"Вы потратили все свои очки"})
				continue

			}

			if _, ok := mathg.Numbers[vkobject.Message.FromID]; ok {
				mathg.Numbers[vkobject.Message.FromID] = append(mathg.Numbers[vkobject.Message.FromID], nums)
			} else {
				mathg.Numbers[vkobject.Message.FromID] = [][]int{nums}
			}
			client.SendDefaultUsMessage(vkobject.Message.FromID, "", &mesid, []interface{}{"Вы успешно выбрали тикет"})
			log.Printf("%s успешно выбрал тикет %s", client.GetName(vkobject.Message.FromID), strnums)

		case "-результаты":

			if vkobject.Message.FromID != 251854093 || !mathg.Active {
				client.SendDefaultUsMessage(vkobject.Message.FromID, "", &mesid, []interface{}{"У вас нет доступа или процесс еще не запущен"})
				continue
			}
			mathg.Active = false

			winnums := mymath.GetRandomVal(mathg.Samplecount, mathg.Range)
			fmt.Printf("Значение %v\n", winnums)
			for id, tk := range mathg.Numbers {
				for _, userticket := range tk {
					for j, usernum := range userticket {
						if !func() bool {
							for _, winnum := range winnums {
								if usernum == winnum {
									return true
								}
							}
							return false
						}() {
							client.SendDefaultUsMessage(id, "", &mesid, []interface{}{fmt.Sprintf(
								"Ваш тикет %v больше не учавствует\n", userticket)})
							fmt.Printf("Тикет %v участника %s больше не учавствует\n", userticket, client.GetName(id))
							break
						}
						if j == len(userticket)-1 {
							client.SendPhotoUsMessage(id, "", &mesid, "winner.jpg", []interface{}{fmt.Sprintf(
								"Ваш тикет %v вышел  с совпавшими %d числами\n", userticket, len(userticket))})
							fmt.Printf("Тикет %v участника %s вышел  с совпавшими %d числами\n", userticket, client.GetName(id), len(userticket))
						}
					}
				}
			}
		}
	}
}

func main() {
	client, err := vk.NewClientWithOptions(vk.WithToken(
		"1f183c054754c05c1d595faabbbf0001eef36a99265568b78edd0c6f0b30837a1b4eb98d1a0ecacf5e7fe"),
		vk.WithHTTPClient(&http.Client{Timeout: time.Second * 30}))
	if err != nil {
		log.Fatal("Error during creating NewClient")
	}

	lp := longpoll.GetLongPoll()
	lp.AddVKLongPoll(client, 202265867)
	steam := lp.GetStream()

	postconf := fmt.Sprintf("host= %s port =%d user= %s password= %s dbname= %s sslmode=disable", host, port, user, password, dbname)
	db, err := sql.Open("postgres", postconf)
	vkclient := chat.VKClient{Client: client}
	vkobjectch := make(chan *longpoll.VKObject, 1000)
	go mathrunner(&vkclient, db, vkobjectch)

	for {
		select {
		case newupdate := <-steam.Updates:
			switch content := newupdate.(type) {
			case *longpoll.VKObject:
				vkobjectch <- content
			}
		case newerror := <-steam.Errors:
			log.Println(newerror)

		}
	}
}
